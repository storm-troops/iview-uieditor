import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { IViewUieditor } from './iview-uieditor'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';

Vue.config.productionTip = false
Vue.use(IViewUieditor);
Vue.use(ViewUI, {
  transfer: true,
  size: 'large',
  capture: false,
  select: {
    arrow: 'md-arrow-dropdown',
    arrowSize: 20
  }
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
